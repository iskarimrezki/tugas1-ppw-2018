from django.conf.urls import url
from add_friends.views import index,add_friend,remove_friend

urlpatterns = [
    url(r'^$' , index, name='index'),
    url(r'^add_friend', add_friend, name='add_friend'),
    url(r'^remove_friend/(?P<obj_id>[0-9]+)',remove_friend, name='remove_friend'),
]
