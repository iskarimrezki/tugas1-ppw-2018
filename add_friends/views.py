from django.shortcuts import render
from django.http import HttpResponseRedirect
from add_friends.models import AddFriends
from add_friends.forms import AddFriendsForm

# Create your views here.
response = {}
def index(request):
    response['add_friends_form'] = AddFriendsForm
    added = AddFriends.objects.all()
    response['added'] = added
    html = 'add_friends.html'
    return render(request,html,response)

def add_friend(request):
    form = AddFriendsForm(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        added = AddFriends(name=response['name'],url=response['url'])
        added.save()
        return HttpResponseRedirect('/add-friends/')
    else:
        return HttpResponseRedirect('/add-friends/')

def remove_friend(request,obj_id):
    friend = AddFriends.objects.get(pk=obj_id)
    friend.delete()
    return HttpResponseRedirect('/add-friends/')